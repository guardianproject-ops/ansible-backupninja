#!/bin/sh
#
# This is a backupninja handler for backing up volumes from a docker container
#
# Abel Luck <abel@guardianproject.info>
# https://gitlab.com/guardianproject-ops/ansible-backupninja
#

getconf backupdir /var/backups
getconf backupimage busybox:latest
getconf container
getconf volumepath

[ -f "/usr/lib/backupninja/docker-functions" ] || fatal "backupninja docker functions are not installed"
. /usr/lib/backupninja/docker-functions

if ! [ -x "$(command -v docker)" ]; then
  fatal "'docker' is not available in the PATH. Is it installed?"
fi

docker_containers_exists $container || fatal "The container '$container' does not exist."
if ! [ docker_images_exists $image ]; then
  docker pull $image || fatal "The image '$image' does not exist and could not be pulled"
fi
(docker inspect -f '{{ .Mounts }}' $container | grep $volumepath ) || fatal "The container '$container' does not have a volume named '$volumepath'"

# create backup dir
[ -d $backupdir ] || (debug "mkdir -p $backupdir"; mkdir -p $backupdir)
[ -d $backupdir ] || fatal "Backup directory '$backupdir' does not exist, and could not be created."

outputfile="$backupdir/$container.tar"
tarstr="tar --create  $volumepath > $outputfile"
execstr="docker run --rm --volumes-from $container:ro $backupimage $tarstr"

debug "$execstr"

if [ ! $test ]; then
  output=`eval $execstr 2>&1`
  code=$?
  if [ "$code" == "0" ]; then
    debug "$output"
    info "Successfully created archive of volume '$volumepath' on container '$container' to $outputfile"
  else
    warning "$output"
    warning "Failed to create archive of volume '$volumepath' on container '$container' to $outputfile"
  fi
fi

return 0
