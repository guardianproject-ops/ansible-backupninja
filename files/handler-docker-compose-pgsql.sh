#!/bin/sh
#
# This is a backupninja handler for postgresql instances running in docker.
#
# Abel Luck <abel@guardianproject.info>
# https://gitlab.com/guardianproject-ops/ansible-backupninja
#

getconf backupdir /var/backups/postgres
getconf service
getconf projectdir
getconf user
getconf dbname
getconf project

[ -f "/usr/lib/backupninja/docker-functions" ] || fatal "backupninja docker functions are not installed"
. /usr/lib/backupninja/docker-functions

if [ -n "$project" ]; then
  DC="docker-compose -p $project"
else
  DC="docker-compose"
fi

DCEXEC="$DC exec -T"

[ -d $projectdir ] || fatal "docker-compose project directory '$projectdir' does not exist."

if ! [ -x "$(command -v docker-compose)" ]; then
  fatal "'docker-compose' is not available in the PATH. Is it installed?"
fi

cd $projectdir

[ -f "docker-compose.yml" ] || fatal "There is no docker-compose.yml file in '$projectdir'."

docker_services_running $service || fatal "The service '$service' is not running (using project '$project' at '$projectdir')."

# create backup dir
[ -d $backupdir ] || (debug "mkdir -p $backupdir"; mkdir -p $backupdir)
[ -d $backupdir ] || fatal "Backup directory '$backupdir' does not exist, and could not be created."

outputfile="$backupdir/${project}-${service}-${dbname}.sql"
execstr="$DCEXEC $service pg_dump -U $user $dbname > '$outputfile'"

debug "$execstr"

if [ ! $test ]; then
  output=`eval $execstr 2>&1`
  code=$?
  if [ "$code" == "0" ]; then
    debug "$output"
    info "Successfully finished dump of pgsql database ${db}"
  else
    warning "$output"
    warning "Failed to dump pgsql database ${db}."
  fi
fi

return 0
