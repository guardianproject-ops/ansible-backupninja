#
# Handy functions for working with docker containers and compose projects
#
# Abel Luck <abel@guardianproject.info>
# https://gitlab.com/guardianproject-ops/ansible-backupninja
#


##
## Outputs the docker container id of the container with name of the first
## argument and returns 1. If the container does not exist, returns 1. Also
## returns 1 if no argument is given.
##
docker_service_id() {
   [ $# -ge 1 ] || return 1
   local service="$1"
   local execstr
   execstr="$DC ps -q $service"
   debug $execstr
   res=$(eval $execstr)
   if [ "$?" == "0" ]; then
     echo $res
   else
     return 1
   fi
}

##
## If all the arguments are running services names, returns 0.
## Else, returns 1. Also returns 1 if no argument is given.
##
docker_services_running() {
   [ $# -ge 1 ] || return 1
   local args="$1"
   local service
   local serviceid
   local execstr
   for service in $args ; do
     debug $DC
     serviceid=$(docker_service_id $service)
     if [ "$?" != "0" ]; then
       return 1
     fi
     execstr="docker ps -q --no-trunc | grep $serviceid"
     debug $execstr
     if [ -z $(eval $execstr) ]; then
       return 1
     fi
   done
   return 0
}

##
## If all the arguments are containers that exist (regardless of running state),
## returns 0. Else, returns 1. Also returns 1 if no argument is given.
##
docker_containers_exists() {
   [ $# -ge 1 ] || return 1
   local args="$1"
   local container
   for container in $args ; do
     execstr="docker ps -a -q -f name='$container'"
     debug $execstr
     res=$(eval $execstr)
     if [ "$?" != "0" ]; then
       return 1
     fi
   done
   return 0
}

##
## If all the arguments are images that exist returns 0.
## Else, returns 1. Also returns 1 if no argument is given.
##
docker_images_exists() {
   [ $# -ge 1 ] || return 1
   local args="$1"
   local image
   for image in $args ; do
     execstr="docker image inspect $image"
     debug $execstr
     res=$(eval $execstr)
     if [ "$?" != "0" ]; then
       return 1
     fi
   done
   return 0
}
